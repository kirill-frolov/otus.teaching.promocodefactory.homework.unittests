﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerPromoCodeLimitBuilder
    {
        private Guid _id;
        private Guid _partnerId;
        private DateTime _createDate;
        private DateTime? _cancelDate;
        private DateTime _endDate;
        private int _limit;

        public PartnerPromoCodeLimitBuilder()
        {

        }

        public PartnerPromoCodeLimitBuilder Id(Guid id)
        {
            _id = id;
            return this;
        }

        public PartnerPromoCodeLimitBuilder PartnerId(Guid partnerId)
        {
            _partnerId = partnerId;
            return this;
        }
        public PartnerPromoCodeLimitBuilder CreateDate(DateTime date)
        {
            _createDate = date;
            return this;
        }

        public PartnerPromoCodeLimitBuilder CancelDate(DateTime? date)
        {
            _cancelDate = date;
            return this;
        }


        public PartnerPromoCodeLimitBuilder EndDate(DateTime date)
        {
            _endDate = date;
            return this;
        }
        public PartnerPromoCodeLimitBuilder Limit(int limit)
        {
            _limit = limit;
            return this;
        }

        public PartnerPromoCodeLimit Build()
        {
            return new PartnerPromoCodeLimit
            {
                Id = (_id == Guid.Empty) ? Guid.NewGuid() : _id,
                PartnerId = _partnerId,
                CreateDate = _createDate,
                CancelDate = _cancelDate,
                EndDate = _endDate,
                Limit = _limit,

            };
        }
    }
}
