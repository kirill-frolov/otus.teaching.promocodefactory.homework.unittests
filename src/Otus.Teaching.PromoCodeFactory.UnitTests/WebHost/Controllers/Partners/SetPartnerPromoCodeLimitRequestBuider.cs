﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{

    public class SetPartnerPromoCodeLimitRequestBuider
    {
        private DateTime _endDate;
        private int _limit;

        public SetPartnerPromoCodeLimitRequestBuider EndDate(DateTime date)
        {
            _endDate = date;
            return this;
        }
        public SetPartnerPromoCodeLimitRequestBuider Limit(int limit)
        {
            _limit = limit;
            return this;
        }

        public SetPartnerPromoCodeLimitRequest Build()
        {
            return new SetPartnerPromoCodeLimitRequest
            {
                EndDate = _endDate,
                Limit = _limit
            };
        }
    }
}
