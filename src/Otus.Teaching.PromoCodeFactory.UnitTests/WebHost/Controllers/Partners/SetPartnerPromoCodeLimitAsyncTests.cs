﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using FluentAssertions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests: IClassFixture<InMemoryDbFixture>
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private InMemoryDbFixture _inMemoryDbFixture;

        public SetPartnerPromoCodeLimitAsyncTests(InMemoryDbFixture inMemoryDbFixture)
        {
            _inMemoryDbFixture = inMemoryDbFixture;
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();

        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partner = new PartnerBuilder().Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, null);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_PropmoCodesCountEqualsZero()
        {
            // Arrange
            var partnerLimit = new PartnerPromoCodeLimitBuilder().Build();
            var partner = new PartnerBuilder()
                .NumberIssuedPromoCodes(100)
                .IsActive(true).Build();
            partner.PartnerLimits.Add(partnerLimit);
            var request = new SetPartnerPromoCodeLimitRequestBuider()
                .EndDate(DateTime.Now.AddDays(10))
                .Limit(1)
                .Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ActiveLimitIsNull_PropmoCodesCountNotChange()
        {
            // Arrange
            var partnerLimit = new PartnerPromoCodeLimitBuilder()
                .CancelDate(DateTime.Now.AddDays(10))
                .Build();
            var partner = new PartnerBuilder()
                .IsActive(true)
                .NumberIssuedPromoCodes(100)
                .Build();
            partner.PartnerLimits.Add(partnerLimit);
            var request = new SetPartnerPromoCodeLimitRequestBuider()
                .EndDate(DateTime.Now.AddDays(10))
                .Limit(1)
                .Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(100);
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_ActiveLimitSwitchOff()
        {
            // Arrange
            var partnerLimit = new PartnerPromoCodeLimitBuilder()
                .Build();
            var partner = new PartnerBuilder()
                .IsActive(true)
                .NumberIssuedPromoCodes(100)
                .Build();
            partner.PartnerLimits.Add(partnerLimit);
            var request = new SetPartnerPromoCodeLimitRequestBuider()
                .EndDate(DateTime.Now.AddDays(10))
                .Limit(1)
                .Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partnerLimit.CancelDate.Value.Date.Should().Be(DateTime.Now.Date);
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitLessThenZero_ReturnsBadRequest()
        {
            // Arrange
            var partner = new PartnerBuilder()
                .IsActive(true)
                .NumberIssuedPromoCodes(100)
                .Build();
            var request = new SetPartnerPromoCodeLimitRequestBuider()
                .Limit(-1)
                .Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_AddLimitToPartner_LimitAddedToDbs()
        {
            // Arrange
            var dbInitializer = _inMemoryDbFixture.ServiceProvider
                .GetService<IDbInitializer>();
            dbInitializer.InitializeDb();
            var partnerRepository = _inMemoryDbFixture.ServiceProvider
                .GetService<IRepository<Partner>>();
            
            var partnersController = new PartnersController(partnerRepository);
            var request = new SetPartnerPromoCodeLimitRequestBuider()
             .EndDate(DateTime.Now.AddDays(10))
             .Limit(1)
             .Build();
            var partnerId = new Guid("7d994823-8226-4273-b063-1a95f3cc1df8");
            var partner = await partnerRepository.GetByIdAsync(partnerId);
            var limitsCnt = partner.PartnerLimits.Count();
            
            // Act
            await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            var partnerCheck = await partnerRepository.GetByIdAsync(partnerId);
            partnerCheck.PartnerLimits.Count().Should().BeGreaterThan(limitsCnt);

        }

    }
}