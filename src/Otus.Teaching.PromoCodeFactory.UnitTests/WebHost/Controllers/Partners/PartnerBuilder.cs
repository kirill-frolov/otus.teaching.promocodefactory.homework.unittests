﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    internal class PartnerBuilder
    {
        private Guid _id;
        private bool _isActive;
        private int _numberIssuedPromoCodes;
        private string _name;
        public PartnerBuilder()
        {

        }

        public PartnerBuilder Id(Guid id)
        {
            _id = id;
            return this;
        }

        public PartnerBuilder IsActive(bool active)
        {
            _isActive = active;
            return this;
        }

        public PartnerBuilder Name(string name)
        {
            _name = name;
            return this;
        }

        public PartnerBuilder NumberIssuedPromoCodes(int count)
        {
            _numberIssuedPromoCodes = count;
            return this;
        }

        public Partner Build()
        {
            return new Partner
            {
                Id = (_id == Guid.Empty) ? Guid.NewGuid() : _id,
                Name = _name,
                NumberIssuedPromoCodes = _numberIssuedPromoCodes,
                IsActive = _isActive,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
        }



    }
}
